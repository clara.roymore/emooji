# Emoojis

<span style="display:block;text-align:center">![Emoojis](examples.png)</span>

A collection of Moo-themed emojis that I made for my Discord server a while back.
I'm sure they're good for other things too.

Included are the classics:

- Smile
- Wow
- Love
- Nervous
- Derp
- Squish
- UwU
- OwO
- Cat
- Cry
- Grump
- Angry

